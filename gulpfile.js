var gulp = require('gulp');
var concat = require('gulp-concat');

var paths = {
  'bower': 'assets/components/',
  'fonts': 'app/fonts/',
  'css': 'app/css/',
  'controllers': 'app/controllers/',
  'js': 'app/js/'
}

jsFiles = [
  paths.bower + 'jquery/dist/jquery.min.js',
  paths.bower + 'bootstrap/dist/js/bootstrap.min.js',
  paths.bower + 'jquery-ui/jquery-ui.js',
  paths.bower + 'angularjs/angular.min.js',
  paths.bower + 'angular-route/angular-route.min.js',

  paths.bower + 'angular-bootstrap/ui-bootstrap.min.js',
  paths.bower + 'bootstrap/js/collapse.js',

  paths.bower + 'lightgallery/dist/js/lightgallery.js',
  paths.bower + 'bootstrap-star-rating/js/star-rating.js',
  paths.bower + 'bootstrap3-dialog/dist/js/bootstrap-dialog.js',
  paths.bower + 'jquery-bar-rating/jquery.barrating.js',

  paths.js + 'constants.js',
  paths.js + 'search.js',
  paths.js + 'app.js',
  paths.controllers + 'navigation.js',
  paths.controllers + 'venues.js',
  paths.controllers + 'index.js',
  paths.controllers + 'reviews.js',
  paths.controllers + 'stories.js',
  paths.controllers + 'add_business.js',
  paths.controllers + 'contact.js',
  paths.controllers + 'jobs.js',
  paths.js + 'search.js'
];

cssFiles = [
  paths.bower + 'bootstrap/dist/css/bootstrap.css',
  paths.bower + 'jquery-ui/themes/base/jquery-ui.min.css',
  paths.bower + 'fontawesome/css/font-awesome.css',

  paths.css + 'fonts.css',

  paths.bower + 'yamm3/yamm/yamm.css',

  paths.css + 'responsive.css',
  paths.css + 'services.css',
  paths.css + 'Glyphter.css',
  paths.css + 'flaticon.css',
  paths.css + 'highlight.css',
  paths.css + 'parent-ids.css',
  paths.css + 'pricicon.css',

  paths.bower + 'magnific-popup/dist/magnific-popup.css',
  paths.bower + 'bootstrap-select/dist/css/bootstrap-select.css',
  paths.bower + 'fakeloader/fakeLoader.css',
  paths.bower + 'bootstrap-star-rating/css/star-rating.css',
  paths.bower + 'summernote/dist/summernote.css',
  paths.bower + 'bootstrap3-dialog/dist/css/bootstrap-dialog.css',
  paths.bower + 'jquery.smartbanner/jquery.smartbanner.css',

  paths.bower + 'cropper/dist/cropper.css',
  paths.bower + 'bxslider-4/dist/jquery.bxslider.css',
  paths.bower + 'slick-carousel/slick/slick.css',
  paths.bower + 'slick-carousel/slick/slick-theme.css',

  paths.bower + 'lightgallery/dist/css/lightgallery.css',
  paths.bower + 'lightgallery/dist/css/lg-transitions.css',

  paths.bower + 'selectize/dist/css/selectize.bootstrap3.css',


  paths.css + 'app.css'
];

fontsFiles = [
  paths.fonts + 'Glyphter.eot',
  paths.fonts + 'flaticon.eot',
  paths.fonts + 'highlight.eot',
  paths.fonts + 'parent-ids.eot',
  paths.fonts + 'pricicon.eot',

  paths.fonts + 'Glyphter.svg',
  paths.fonts + 'flaticon.svg',
  paths.fonts + 'highlight.svg',
  paths.fonts + 'parent-ids.svg',
  paths.fonts + 'pricicon.svg',

  paths.fonts + 'Glyphter.ttf',
  paths.fonts + 'flaticon.ttf',
  paths.fonts + 'highlight.ttf',
  paths.fonts + 'parent-ids.ttf',
  paths.fonts + 'pricicon.ttf',

  paths.fonts + 'Glyphter.woff',
  paths.fonts + 'flaticon.woff',
  paths.fonts + 'highlight.woff',
  paths.fonts + 'parent-ids.woff',
  paths.fonts + 'pricicon.woff',

  paths.bower + 'fontawesome/fonts/FontAwesome.otf',
  paths.bower + 'fontawesome/fonts/fontawesome-webfont.eot',
  paths.bower + 'fontawesome/fonts/fontawesome-webfont.svg',
  paths.bower + 'fontawesome/fonts/fontawesome-webfont.ttf',
  paths.bower + 'fontawesome/fonts/fontawesome-webfont.woff',
  paths.bower + 'fontawesome/fonts/fontawesome-webfont.woff2',

  paths.bower + 'bootstrap/fonts/glyphicons-halflings-regular.eot',
  paths.bower + 'bootstrap/fonts/glyphicons-halflings-regular.svg',
  paths.bower + 'bootstrap/fonts/glyphicons-halflings-regular.ttf',
  paths.bower + 'bootstrap/fonts/glyphicons-halflings-regular.woff',
  paths.bower + 'bootstrap/fonts/glyphicons-halflings-regular.woff2',

];





// create task
gulp.task('js', function(){
  gulp.src(jsFiles)
      .pipe(concat('application.js'))
      .pipe(gulp.dest('assets/js'))
});

gulp.task('css', function(){
  gulp.src(cssFiles)
      .pipe(concat('application.css'))
      .pipe(gulp.dest('assets/css'))
});

gulp.task('fonts', function(){
  gulp.src(fontsFiles)
      .pipe(gulp.dest('assets/fonts'))
});

gulp.task('default', ['js','css','fonts']);
