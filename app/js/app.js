var fabogoApp = angular.module('fabogoApp', ['ngRoute', 'ui.bootstrap']);
var fabogoRouteProvider;
var siteApiRoot = 'http://fabogoSandbox-dev.ap-northeast-1.elasticbeanstalk.com/';
// var siteApiRoot = 'http://127.0.0.1:8000/';

fabogoApp.service('sharedParams', function ($http) {
  var currentCity = { slug:'dubai' };
  var currentSubzone = null;
  var currentCategory = { name:'Salons and Spas', slug:'salons-and-spas' };
  var currentServices = [];

  var citiesList = [];
  var servicesList = [];
  var categoriesList = [];
  var highlightsList = [];
  var genderHighlightsList = [];
  var priceList = [];
  var rateables = [];
  var subzonesList = [];

  var citiesSlugList = [];
  var servicesSlugList = [];
  var categoriesSlugList = [];
  var currentChainSlug = null;
  var currentGroupSlug = null;
  var seoParams = {title:'Salons and Spas', desc:'Salons and Spas'};

  url = '/app/data/assets.json';//siteApiRoot+'api/base/web/site/assets';

  var promise =   $http.post(url, jQuery.param([]), {
      method: "POST",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    }
  ).success(function(data){
    categoriesList = data.data.categories;
    categoriesList.push({ name:'Salons and Spas', slug:'salons-and-spas'});

    currentCity = data.data.current_city;

    subzonesList = data.data.zones;
    highlightsList = data.data.highlights;
    genderHighlightsList = data.data.genders;
    servicesList = data.data.services;
    citiesList = data.data.cities;
    priceList = data.data.prices;
    rateables = data.data.rateables;
    highlightsIcons = data.data.highlights_icons;

    g_categories = data.data.categories;
    for(i in g_categories){
      g_categories[i].icon = '';
      g_categories[i].type = 'CATEGORY';
    }
    g_zones = data.data.zones;
  });


  return {
    promise:promise,
    getChainSlug: function() {
      return currentChainSlug;
    },
    getGroupSlug: function() {
      return currentGroupSlug;
    },

    setChainSlug: function(newSlug) {
      currentChainSlug = newSlug;
    },
    setGroupSlug: function(newSlug) {
      currentGroupSlug = newSlug;
    },
    getSEOParameter: function (seoIndex) {
      return seoParams[seoIndex];
    },

    getSubzones: function () {
      return subzonesList;
    },
    getSubzone: function () {
      return currentSubzone == null ? currentCity : currentSubzone;
    },
    setSubzone: function(newSubzone) {
      console.log('setting stuff here', newSubzone);

      currentSubzone = newSubzone;
    },

    getSubzoneFromSlug: function (slug) {
      for(i in subzonesList){
        if(subzonesList[i].slug == slug){
          return subzonesList[i];
        }
      }
      return null;
    },

    hasCitiesSlug: function (slug) {
      return citiesSlugList.indexOf(slug) > 0;
    },
    getCitiesSlugs: function () {
      return citiesSlugList;
    },
    getCities: function () {
      return citiesList;
    },
    setCities: function(newCities) {
      citiesList = newCities;
      citiesSlugList = [];
      for(i in citiesList){
        citiesSlugList.push(citiesList[i].slug);
      }
    },

    hasServicesSlug: function (slug) {
      return servicesSlugList.indexOf(slug) > 0;
    },
    getServicesSlugs: function () {
      return servicesSlugList;
    },
    getServices: function () {
      return servicesList;
    },

    getServicesFromSlug: function (slugs) {
      if(slugs){

      }else{
        slugs = '';
      }

      slugs = slugs.split(',');

      services_list = [];
      for(i in servicesList){
        if(slugs.indexOf(servicesList[i].slug) >= 0){
          services_list.push(servicesList[i]);

        }
      }
      return services_list;
    },

    setServices: function(newServices) {
      servicesList = newServices;
      servicesSlugList = [];
      for(i in servicesList){
        servicesSlugList.push(servicesList[i].slug);
      }
    },

    getPrices: function(){
      return priceList;
    },
    setPrices: function(newPrices) {
      priceList = newPrices;
    },

    getRateables: function(){
      return rateables;
    },
    setRateables: function(newRateables) {
      rateables = newRateables;
    },


    getHighlightsIcons: function(){
      return highlightsIcons;
    },
    setHighlightsIcons: function(newHighlightsIcons) {
      highlightsIcons = newHighlightsIcons;
    },
    getHighlightIconFromSlug: function(slug){
      return highlightsIcons[slug];
    },

    getCurrentPageType: function($routeParams){
      city_slug = $routeParams['city'];
      category_slug = $routeParams['category'];
      services_slug = $routeParams['services'];

      routed = 'list';

      if(category_slug=='salons-spas-and-fitness-chain'){
        chain_slug = services_slug;
        category_slug = 'salons-and-spas';
        this.setChainSlug(chain_slug);
        routed = 'list';
      }else{
        this.setChainSlug(null);
      }

      if(services_slug=='outlets'){
        group_slug = category_slug;
        services_slug = '';
        this.setGroupSlug(group_slug);
        routed = 'list';
      }else{
        this.setGroupSlug(null);
      }

      // if the category slug doesn't end with a category 
      // or isn't a category plural then its definitely a slug

      if((this.getGroupSlug() == null) && 
          (this.getChainSlug() == null) &&
            (!this.categoriesExistInSlug(category_slug))){
        routed = 'single';
      }

      return routed;

    },


    getGenderHighlights: function(){
      return genderHighlightsList;
    },
    setGenderHighlights: function(newGenderHighlights) {
      genderHighlightsList = newGenderHighlights;
    },

    getHighlights: function(){
      return highlightsList;
    },
    setHighlights: function(newHighlights) {
      highlightsList = newHighlights;
    },

    hasCategoriesSlug: function (slug) {
      return categoriesSlugList.indexOf(slug) > 0;
    },
    getCategoriesFromSlug: function (slug) {

      category_slug = '-salons-and-spas';

      if(slug.endsWith(category_slug)){
        // get the subzone slug here
        subzone_slug = slug.slice(0, (slug.length - category_slug.length));
        // set the subzone
        this.setSubzone(this.getSubzoneFromSlug(subzone_slug));
        return { name:'Salons and Spas', slug:'salons-and-spas' };
      }

      category_slug = 'salons-and-spas';

      if(slug == category_slug){
        return { name:'Salons and Spas', slug:'salons-and-spas' };
      }

      for(i in categoriesList){
        category_slug = categoriesList[i].slug+'s';

        if(category_slug == slug){
          return categoriesList[i];
        }
        // maybe its part of the slug i.e. subzone-categorys
        category_slug = '-'+category_slug;
        if(slug.endsWith(category_slug)){
          category = categoriesList[i];
          // get the subzone slug here
          subzone_slug = slug.slice(0, (slug.length - category_slug.length));
          // set the subzone
          this.setSubzone(this.getSubzoneFromSlug(subzone_slug));
          return category;
        }
      }

      return { name:'Salons and Spas', slug:'salons-and-spas' };
    },

    categoriesExistInSlug: function (slug) {

      category_slug = '-salons-and-spas';

      if(slug.endsWith(category_slug)){
        return true;
      }

      category_slug = 'salons-and-spas';

      if(slug == category_slug){
        return true;
      }

      for(i in categoriesList){
        category_slug = categoriesList[i].slug+'s';

        if(category_slug == slug){
          return true;
        }
        // maybe its part of the slug i.e. subzone-categorys
        category_slug = '-'+category_slug;
        if(slug.endsWith(category_slug)){
          return true;
        }
      }

      return false;
    },


    getCategoriesSlugs: function () {
      return categoriesSlugList;
    },
    getCategories: function () {
      return categoriesList;
    },
    setCategories: function(newCategories) {
      categoriesList = newCategories;
      categoriesSlugList = [];
      for(i in categoriesList){
        categoriesSlugList.push(categoriesList[i].slug+'s');
      }
    },
    getCurrentServices: function () {
      return currentServices;
    },
    setCurrentServices: function(newServices) {
      currentServices = newServices;
    },
    getCurrentCategory: function () {
      return currentCategory;
    },
    setCurrentCategory: function(newCategory) {
      currentCategory = newCategory;
    },
    getCurrentCity: function () {
      return currentCity;
    },
    getCityFromSlug: function (slug) {
      for(i in citiesList){
        if(citiesList[i].slug == slug){
          return citiesList[i];
        }
      }
      return false;
    },

    setCurrentCity: function(newCity) {
      currentCity = newCity;
    }
  };
});


fabogoApp.config(['$routeProvider', '$locationProvider', 
                      function($routeProvider, $locationProvider) {
  $locationProvider.html5Mode({  
    enabled: true,
    requireBase: false
  });

  fabogoRouteProvider = $routeProvider;

  fabogoRouteProvider.when('/', {
    templateUrl : '../../app/templates/home.html',
    controller  : 'IndexController',
    resolve:{
      sharedParamsData:function(sharedParams){
        return sharedParams.promise;
      }
    }
  }).when('/jobs', {
    templateUrl : '../../app/templates/jobs/list.html',
    controller  : 'JobsController',
    resolve:{
      sharedParamsData:function(sharedParams){
        return sharedParams.promise;
      },
    }
  }).when('/about', {
    templateUrl : '../../app/templates/pages/about.html',
    resolve:{
      sharedParamsData:function(sharedParams){
        return sharedParams.promise;
      },
    }
  }).when('/add-your-business', {
    templateUrl : '../../app/templates/pages/add-business.html',
    controller  : 'AddBusinessController',
    resolve:{
      sharedParamsData:function(sharedParams){
        return sharedParams.promise;
      },
    }
  }).when('/contact', {
    templateUrl : '../../app/templates/pages/contact.html',
    controller  : 'ContactController',
    resolve:{
      sharedParamsData:function(sharedParams){
        return sharedParams.promise;
      },
    }
  }).when('/stories/:about?', {
    templateUrl : '../../app/templates/stories/list.html',
    controller  : 'StoriesController',
    resolve:{
      sharedParamsData:function(sharedParams){
        return sharedParams.promise;
      },
    }
  }).when('/:city/:category/:services?', {
    templateUrl : '../../app/templates/container.html',
    controller  : 'VenuesController',
    resolve:{
      sharedParamsData:function(sharedParams){
        return sharedParams.promise;
      },
    }
  }).otherwise( {
    redirect: '/'
  });


}]);