fabogoApp.controller('VenuesController', 
  function($scope, $http, $log, $routeParams, $route, sharedParams){

    $scope.includable = '';

    var vCtrl = this;
    var sharedParams = sharedParams;


    // single venue page variables
    vCtrl.currentVenue = {};
    vCtrl.currentVenueTimings = [];
    vCtrl.currentVenuePhotos = [];
    vCtrl.currentVenueRateCards = [];
    vCtrl.currentVenueServices = [];
    vCtrl.currentVenueReviews = [];
    vCtrl.currentSuggestedVenues = [];

    // common elements and multiple displayable venues show here
    vCtrl.venues = [];
    vCtrl.filters_applied = [];
    vCtrl.ads = [];
    vCtrlcategories = [];
    vCtrl.breadcrumbs = [];

    filters = {};
    filters.highlights = sharedParams.getHighlights();
    filters.genders = sharedParams.getGenderHighlights();
    filters.categories = sharedParams.getCategories();
    filters.zones = sharedParams.getSubzones();
    filters.services = sharedParams.getServices();
    filters.prices = sharedParams.getPrices();

    filters.sortables = sharedParams.getRateables();
    filters.currentSort = filters.sortables['popularity'];
    
    vCtrl.filters = filters;

    current_services = sharedParams.getCurrentServices();
    current_service_ids = [];

    for(i in current_services){
      current_service_ids.push(current_services[i].id)
    }

    vCtrl.selects = {  
                      highlights:[], 
                      prices:[], 
                      categories:[], 
                      serviceIds:current_service_ids,
                      services:current_services,
                      category:sharedParams.getCurrentCategory(), 
                      zone:sharedParams.getSubzone()
                    };
    vCtrl.totalItems = 10;
    vCtrl.currentPage = 1;
    vCtrl.maxSize = 8;
    vCtrl.numPerPage = 20;
    vCtrl.totalPages = 0;
    vCtrl.initialized  = 0;

    vCtrl.titleText = 'Salons and Spas';
    vCtrl.seoTitle = 'Salons and Spas in  - Fabogo';
    vCtrl.session_search_page = '';
    vCtrl.session_search_parameters='';

    $scope.$on('$routeChangeSuccess', function() {
      vCtrl.setVariablesFromCurrentUrl();
    });

    vCtrl.resetCurrentPage = function(){
      vCtrl.currentPage = 1;
    }

    // link generators

    vCtrl.mzk_city_slug = function(params){
      city = sharedParams.getCurrentCity()
      url = '/' + city['slug'] + '/';
      append = 'salons-and-spas';
      if(params.category){
        append = params.category.slug + 's';
      }else{
        if(params.category!=null){
          currentCategory = sharedParams.getCurrentCategory();
          if(typeof(currentCategory.id)!=='undefined'){
            append = currentCategory.slug + 's';
          }else{
            append = currentCategory.slug;
          }
        }
      }
      if(params.subzone){
        if(params.subzone.slug!=city['slug']){
          append = params.subzone.slug + '-' + append;
        }
      }else{
        currentCity = sharedParams.getCurrentCity();
        currentSubzone = sharedParams.getSubzone();
        if(currentCity.id != currentSubzone.id){
          append = currentSubzone.slug + '-' + append;
        }

      }

      if(params.service){
        if(typeof(params.service) !== "undefined"){
          append = append + '/' + params.service.slug;
        }
      }
      sort = '';
      if(params.sort){
        sort = '?sort='+params.sort.label;
      }

      url = url + append+sort;
      return url;
    };

    vCtrl.mzk_single_slug = function(venue){
      url = '/' + venue.citySlug + '/' + venue.slug;
      return url;
    };

    vCtrl.mzk_single_slug_photos = function(venue){
      url = vCtrl.mzk_single_slug(venue) + '/photos';
      return url;
    };

    vCtrl.mzk_single_slug_rate_cards = function(venue){
      url = vCtrl.mzk_single_slug(venue) + '/rate-card';
      return url;
    };

    // initializations

    vCtrl.init = function () {
      t = sharedParams.getCurrentPageType($routeParams);
      console.log('chk', t);
      if(t == 'list'){
        $scope.includable = '../../app/templates/businesses/list.html';
        vCtrl.initList();
      }else if(t == 'single'){
        $scope.includable = '../../app/templates/businesses/single.html';
        console.log('do that thing');
        vCtrl.initSingle();

      }

    };

    /// initalization for single venues

    vCtrl.initSingle = function(){
      vCtrl.setVariablesFromCurrentUrl();
      vCtrl.setTitleText();
      vCtrl.getVenue();
    }

    /// initalization for list of venues
    vCtrl.initList = function(){
      vCtrl.setVariablesFromCurrentUrl();
      vCtrl.setTitleText();
      vCtrl.filterVenues();
    }

    // helpers filters
    vCtrl.noCategorySelected = function(){
      c = vCtrl.selects.category;
      if(c==null){
        return true;
      }

      if(c.slug=='salons-and-spas'){
        return true;
      }

      return false;
    }

    vCtrl.setVariablesFromCurrentUrl = function(){
      city_slug = $routeParams['city'];
      category_slug = $routeParams['category'];
      services_slug = $routeParams['services'];
      if(category_slug=='salons-spas-and-fitness-chain'){
        chain_slug = services_slug;
        category_slug = 'salons-and-spas';
        sharedParams.setChainSlug(chain_slug);
      }else{
        sharedParams.setChainSlug(null);
      }

      if(services_slug=='outlets'){
        group_slug = category_slug;
        services_slug = '';
        sharedParams.setGroupSlug(group_slug);
      }else{
        sharedParams.setGroupSlug(null);
      }

      console.log($routeParams);
      currentCity = sharedParams.getCityFromSlug(city_slug);
      currentCategory = sharedParams.getCategoriesFromSlug(category_slug);
      currentServices = sharedParams.getServicesFromSlug(services_slug);
      currentSubzone = sharedParams.getSubzone();

      vCtrl.setSelectedService(currentServices);
      vCtrl.setSelectedCategory(currentCategory);
      vCtrl.updateZone(currentSubzone);
      
      sharedParams.setCurrentCity(currentCity);
      sharedParams.setCurrentCategory(currentCategory);
      sharedParams.setCurrentServices(currentServices);
      if((sharedParams.getGroupSlug() == null) && 
          (sharedParams.getChainSlug() == null) &&
            (!sharedParams.categoriesExistInSlug(category_slug))){
        vCtrl.setCurrentVenueSlug(category_slug);
      }

      //vCtrl.filterVenues();
    }

    var getHighlightCssClass = function(highlight_slug){
      return sharedParams.getHighlightIconFromSlug(highlight_slug);
    }

    var getPriceIconCss = function(price_id){
      str = '';
      switch(price_id){
        case 1:
          str = 'economical';
        break;
        case 2:
          str = 'premium';
        break;
        case 3:
          str = 'luxury';
        break;
      };

      return sharedParams.getHighlightIconFromSlug(str);
    };

    var getPriceName = function(price_id){
      str = '';
      switch(price_id){
        case 1:
          str = 'Economical';
        break;
        case 2:
          str = 'Premium';
        break;
        case 3:
          str = 'Luxury';
        break;
      };

      return str;
    };

    vCtrl.isAnyCategorySelected = function(){
      if(vCtrl.selects.category == null){
        return true;
      }
      return false;
    }

    vCtrl.updateZone = function(zone){
      vCtrl.selects.zone = zone;
      vCtrl.resetCurrentPage();
      vCtrl.setTitleText();
    };

    vCtrl.ratingColor = function(rating){
      
    };

    vCtrl.updateSort = function(sortr){
      vCtrl.filters.currentSort = sortr;
    }

    vCtrl.isSortSelected = function(sortr){
      if(vCtrl.filters.currentSort == null){
        return false;
      }

      if(vCtrl.filters.currentSort.label == sortr.label){
        return true;
      }

      return false;
    }


    vCtrl.setSelectedService = function(serviceArray){
      sharedParams.setCurrentServices(serviceArray!=null?serviceArray:[]);
      vCtrl.selects.services = serviceArray;
      serviceIds = [];
      for(i in serviceArray){
        serviceIds.push(serviceArray[i].id);
      }
      vCtrl.selects.serviceIds = serviceIds;
    };

    vCtrl.updateService = function(service){
      vCtrl.setSelectedService(service);
      vCtrl.resetCurrentPage();
      vCtrl.setTitleText();
    };

    vCtrl.isServiceSelected = function(service){
      
      idx = vCtrl.selects.serviceIds.indexOf(service.id);
      if(idx >=0){
        return true;
      }else{
        return false;
      }
      current_services = sharedParams.getCurrentServices();
      for(i in current_services){
        if(current_services[i].id == service.id){
          return true;
        }
      }
      return false;
    }


    vCtrl.setSelectedCategory = function(category){
      vCtrl.selects.category = category;
      sharedParams.setCurrentCategory(category);
      console.log('selected category', vCtrl.selects.category);
    };

    vCtrl.setCurrentVenueSlug = function(slug){
      vCtrl.current_venue_slug = slug;
    }

    vCtrl.updateCategory = function(category){
      vCtrl.setSelectedCategory(category);
      vCtrl.resetCurrentPage();
      vCtrl.setTitleText();
    };

    vCtrl.isCategorySelected = function(category){
      if(vCtrl.selects.category == null){
        return false;
      }
      if(vCtrl.selects.category.id == category.id){
        return true;
      }
      return false;
    }


    vCtrl.setTitleText = function(){
      txt = 'Salons and Spas';
      append = '';
      prepend = '';
      city = sharedParams.getCurrentCity()
      chain_slug = sharedParams.getChainSlug();
      group_slug = sharedParams.getGroupSlug();
      if(chain_slug!=null){
        prepend = 'Outlets of ';
      }

      if(group_slug!=null){
        prepend = 'Outlets of ';
      }

      if(vCtrl.selects.category){
        if(!vCtrl.selects.category.name.endsWith('s')){
          txt = vCtrl.selects.category.name+'s';
        }else{
          txt = vCtrl.selects.category.name;
        }
      }else{
        txt = 'Salons and Spas';
      }
      if(vCtrl.selects.service != null){
        txt = vCtrl.selects.service.name;
      }

      if(vCtrl.selects.zone){
        append = ' in ' + vCtrl.selects.zone.name;
      }else{
        append = ' in ' + vCtrl.city['name'];
      }

      vCtrl.titleText = prepend + txt + append;
      vCtrl.seoTitle = vCtrl.titleText + ' - Fabogo';
    }


    vCtrl.getTitleText = function(){
      return vCtrl.titleText;
    }

    vCtrl.getFilterParameters = function(){
      data = {};
      city = sharedParams.getCurrentCity()
      chain_slug = sharedParams.getChainSlug();
      group_slug = sharedParams.getGroupSlug();

      if(chain_slug!=null){
        data['chain_slug'] = chain_slug;
      }

      if(group_slug!=null){
        data['group_slug'] = group_slug;
      }

      data['batchNumber'] = vCtrl.currentPage;
      data['zoneID'] = vCtrl.selects.zone.id;
      data['cityID'] = city['name'];
      data['queryText'] = '';
      data['costEstimate'] = vCtrl.selects.prices.join(',');
      data['sortBy'] = vCtrl.filters.currentSort.label;
      data['highlightsCSV'] =  vCtrl.selects.highlights.join(',');
      data['categoryIDCSV'] = vCtrl.selects.category != null ? vCtrl.selects.category.id : '';
      data['serviceIDCSV']   =  vCtrl.selects.service?vCtrl.selects.service.id:'';//s.join(',');
      data['session_search_page'] =  vCtrl.session_search_page;
      data['session_search_parameters'] =  vCtrl.session_search_parameters;
      return data;
    }



    vCtrl.updateServices = function(service){
      // return true if added and false if removed
      //idx = vCtrl.filters.genders.indexOf(gender);
      idx = $.inArray(price, vCtrl.selects.services);
      vCtrl.resetCurrentPage();

      if(idx >=0){
        // it exists remove it and return a false;
        vCtrl.selects.services.splice(idx, 1);
        sharedParams.setCurrentServices(vCtrl.selects.services);
        return false;
      }else{
        vCtrl.selects.services.push(service);
        sharedParams.setCurrentServices(vCtrl.selects.services);
        return true;
      }
    };


    vCtrl.isZoneSelected = function(zone){
      if(vCtrl.selects.zone == null){
        return false;
      }

      if(vCtrl.selects.zone.id == zone.id){
        return true;
      }else{
        return false;
      }
    };


    vCtrl.isPriceSelected = function(price){
      // return true if added and false if removed
      //idx = vCtrl.filters.genders.indexOf(gender);
      idx = $.inArray(price, vCtrl.selects.prices);
      if(idx >=0){
        return true;
      }else{
        return false;
      }
    };

    vCtrl.updatePrices = function(price){
      // return true if added and false if removed
      //idx = vCtrl.filters.genders.indexOf(gender);
      idx = $.inArray(price, vCtrl.selects.prices);

      if(idx >=0){
        // it exists remove it and return a false;
        vCtrl.selects.prices.splice(idx, 1);
        vCtrl.resetCurrentPage();
        vCtrl.filterVenues();
        return false;
      }else{
        vCtrl.selects.prices.push(price);
        vCtrl.resetCurrentPage();
        vCtrl.filterVenues();
        return true;
      }
    };


    vCtrl.isGenderSelected = function(gender){
      // return true if added and false if removed
      //idx = vCtrl.filters.genders.indexOf(gender);
      idx = $.inArray(gender, vCtrl.selects.genders);
      if(idx >=0){
        return true;
      }else{
        return false;
      }
    };

    vCtrl.updateGenders = function(gender){
      idx = $.inArray(gender, vCtrl.selects.genders);
      vCtrl.resetCurrentPage();

      if(idx >=0){
        // it exists remove it and return a false;
        vCtrl.selects.genders.splice(idx, 1);
      }else{
        vCtrl.selects.genders.push(gender);
      }

      vCtrl.filterVenues();
    };



    vCtrl.isHighlightSelected = function(highlight){
      // return true if added and false if removed
      //idx = vCtrl.filters.genders.indexOf(gender);
      idx = $.inArray(highlight, vCtrl.selects.highlights);
      if(idx >=0){
        return true;
      }else{
        return false;
      }
    };

    vCtrl.updateHighlights = function(highlight){
      // return true if added and false if removed
      //idx = vCtrl.filters.genders.indexOf(gender);
      idx = $.inArray(highlight, vCtrl.selects.highlights);
      vCtrl.resetCurrentPage();

      if(idx >=0){
        // it exists remove it and return a false;
        vCtrl.selects.highlights.splice(idx, 1);
      }else{
        vCtrl.selects.highlights.push(highlight);
      }
      vCtrl.filterVenues();
    };


    vCtrl.setupBreadCrumbs = function(){
      vCtrl.breadcrumbs = [];
      // add base city
      vCtrl.breadcrumbs.push({label:vCtrl.current_city_name, url:vCtrl.mzk_city_slug({})});
      prms = {};
      if(vCtrl.selects.zone){
        if((vCtrl.selects.zone.name != vCtrl.current_city_name)){
          prms['subzone'] = vCtrl.selects.zone;
          vCtrl.breadcrumbs.push({label:vCtrl.selects.zone.name, 
                                                 url:vCtrl.mzk_city_slug(prms)});
        }
      }

      if(vCtrl.selects.category!=null){
        prms['category'] = vCtrl.selects.category;
        vCtrl.breadcrumbs.push({label:vCtrl.selects.category.name+'s', 
                                               url:vCtrl.mzk_city_slug(prms)});
      }else{
        vCtrl.breadcrumbs.push({label:'Salons and Spas', 
                                               url:vCtrl.mzk_city_slug(prms)});
      }
    };

    // single venue helpers

    vCtrl.setupCurrentVenueTimings = function(timings){
      result = [];
      days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', "Sat"];
      for(i in days){
        current_days_timings = [];
        for(y in timings){
          if(timings.daysOfWeek.search(days[i])>=0){
            current_days_timings.push({
              open:timings[y].open,
              close:timings[y].close,
              open_utc:timings[y].open_utc,
              close_utc:timings[y].close_utc
            })
          }
        }
        if(current_days_timings.length > 0){
          result.push({day:days[i], timings:current_days_timings})
        }
      }

      vCtrl.currentVenueTimings = result;
    }

    vCtrl.toggleFavoriteVenue = function(venue){
      // mark as favorited
    }

    vCtrl.getCurrentVenueAttr = function(attr){
      valid_attrs = ['id','name','description','phone','email','website','geolocated',
                    'zone_id','geolocation_city','geolocation_state','geolocation_country',
                    'geolocation_address','slug','active','facebook','twitter','instagram',
                    'google','chain_id','landmark','created_by_id','geolocation_longitude',
                    'geolocation_latitude','rate_card_count','image_count','reviews_count',
                    'favorites_count','checkins_count','services_count','categories_count',
                    'highlights_count','timings_count','zone_cache','rating_average',
                    'active_deals_count','total_deals_count','total_packages_count',
                    'active_packages_count','total_ratings_count','cost_estimate',
                    'active_offers_count','city_id','ref','lot_id','has_sample_menu',
                    'has_stock_cover_image','popularity','type','is_featured','is_rich',
                    'cover_x_pos','cover_y_pos','is_home_service','facebook_like_box_id',
                    'facebook_like_box_status','pre_right_column_html','aliases','phones',
                    'facebook_like_box_valid_until'];
      if (!(valid_attrs.indexOf(attr)>=0)){
        return '';
      }

      if(typeof(vCtrl.currentVenue[attr]) != "undefined"){
        return vCtrl.currentVenue[attr];
      }

      return '';
    };


    vCtrl.getCurrentVenueCoverPhoto = function(){
      if(typeof(vCtrl.currentVenue.cover) != "undefined"){
        return vCtrl.currentVenue.cover.photo;
      }
      return '';
    };

    vCtrl.getVenue = function(){
      url = siteApiRoot + 'api/searchForWeb';
      url = '/app/data/single-venue.json';

      data = { id:'id-of-venue' };

      $http.post(url, jQuery.param(data), {
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }
      ).success(function(data){
        vCtrl.currentVenue = data.business;
        vCtrl.currentVenueTimings = vCtrl.setupCurrentVenueTimings(data.timings);
        vCtrl.currentVenuePhotos = data.photos;
        vCtrl.currentVenueRateCards = data.rateCards;
        vCtrl.currentVenueServices = data.services;
        vCtrl.currentSuggestedVenues = data.suggested_spas;
        vCtrl.currentVenueReviews = data.reviews;
        vCtrl.ads = data.ads;
        vCtrl.suggestions = data.suggested_spas;
        
        vCtrl.initialized = 1;
        vCtrl.session_search_page = data.debug.session_search_page;
        vCtrl.session_search_parameters = data.debug.session_search_parameters;
        vCtrl.setupBreadCrumbs();

      });
    }


    vCtrl.filterVenues = function(){
      url = siteApiRoot + 'api/searchForWeb';
      url = '/app/data/venues.json';

      data = vCtrl.getFilterParameters();

      $http.post(url, jQuery.param(data), {
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }
      ).success(function(data){

        vCtrl.venues = data.data.businessArray;
        vCtrl.totalItems = data.data.batchDetails.totalNumberOfBusiness;
        vCtrl.currentPage = data.data.batchDetails.batchNumber;
        vCtrl.ads = data.data.AdsArray;
        vCtrl.initialized = 1;
        vCtrl.session_search_page = data.data.debug.session_search_page;
        vCtrl.session_search_parameters = data.data.debug.session_search_parameters;
        vCtrl.setupBreadCrumbs();

      });
    };

    // fire on controller loaded
    vCtrl.init();    

    vCtrl.slugSingle = function(business){
      //return mzk_slug_single(business);
    };

    vCtrl.getFilterableServices = function(){
      return vCtrl.filters.services.slice(1,9);
    };

    vCtrl.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    vCtrl.pageChanged = function() {
      vCtrl.setVariablesFromCurrentUrl();
      vCtrl.filterVenues();
    };

    // jquery handlers go right here
    vCtrl.setupGalleries = function(){
      console.log('all done', $('.rate-card-holder-main'));
      $('.rate-card-holder-main').lightGallery({
        selector: 'a.image:not(.sq-card)',
        download: false,
        getCaptionFromTitleOrAlt: false 
      });

      $('.photos-holder-main').lightGallery({
        selector: 'a.image:not(.sq-card)',
        download: false,
        getCaptionFromTitleOrAlt: false 
      });

      $('.hide-only-mobile .rate-card-holder').lightGallery({
        selector: 'a.image:not(.sq-card)',
        download: false,
        getCaptionFromTitleOrAlt: false 
      });

      $('.hide-only-mobile .photos-holder').lightGallery({
        selector: 'a.image:not(.sq-card)',
        download: false,
        getCaptionFromTitleOrAlt: false 
      });

      $('.show-only-mobile .rate-card-holder').lightGallery({
        selector: 'a.image:not(.sq-card)',
        download: false,
        getCaptionFromTitleOrAlt: false 
      });


      $('.show-only-mobile .photos-holder').lightGallery({
        selector: 'a.image:not(.sq-card)',
        getCaptionFromTitleOrAlt: false,
        download: false
      });


    };



    vCtrl.setupReviewWidget = function(){

      $('#rating').barrating('show', {
        theme: 'bars-pill',
        showClear: true,
        filledStar: '<i class="fa fa-circle"></i>',
        emptyStar: '<i class="fa fa-circle-o"></i>',

        clearButton:'<i class="fa fa-times"></i>',
        size:'xs',
        starCaptions: function(val) {
          if(val <= 1){
            return 'Poor';
          }
          if(val <= 1.5){
            return 'One and half';
          }
          if(val <= 2.5){
            return 'Average';
          }
          if(val <= 3.5){
            return 'Good';
          }
          if(val <= 4.5){
            return 'Very Good';
          }
          if(val <= 5){
            return 'Fabulous';
          }
        }
      });

      $('.review-clear-rating').click(function(){
        $('#rating').barrating('set', "");
      });
    };



    vCtrl.setupRatingWidget = function(){

      $('.review-clear-rating').click(function(){
        $('#rating').barrating('set', "");
      });


    $('#raterupper').barrating({
      showClear: true,
      clearButton:'<i class="fa fa-times"></i>',
      starCaptions: function(val) {
        if(val <= 1){
          return 'Poor';
        }
        if(val <= 1.5){
          return 'One and half';
        }
        if(val <= 2.5){
          return 'Average';
        }
        if(val <= 3.5){
          return 'Good';
        }
        if(val <= 4.5){
          return 'Very Good';
        }
        if(val <= 5){
          return 'Fabulous';
        }

      },
      click:function(value, text) {
        //console.log(value, text);
      },
      onSelect: function(value, caption, event){
        var vl = value;
        if(vl == ''){
          return;
        }
        $.ajax({
          url:'',
          method:'POST',
          data:{rating: value},
          success:function(data){
            $('.review-panel').find('input[name=rating]').barrating('set', vl);
            clr = $('.star-rating').find('.clear-rating').first();
            if($('#raterupper').val() == 0){
              $(clr).addClass('hidden');
            }else{
              $(clr).removeClass('hidden');
            }
          }
        });
     }

    }); 










    };


});

fabogoApp.directive( 'elemReady', function( $parse ) {
   return {
       restrict: 'A',
       link: function( $scope, elem, attrs ) {    
          elem.ready(function(){
            $scope.$apply(function(){
                var func = $parse(attrs.elemReady);
                func($scope);
            })
          })
       }
    }
})

