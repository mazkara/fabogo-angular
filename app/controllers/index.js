fabogoApp.controller('IndexController', function($scope, $http, $log, $routeParams, $route, sharedParams){
  var indexCtrl = this;
  var sharedParams = sharedParams;
  indexCtrl.cityID = 1;

  indexCtrl.city = {name:'', id:1};
  indexCtrl.popularServices = [];
  indexCtrl.featuredVenues = [];
  indexCtrl.latestPosts = [];

  indexCtrl.assets_url = function(url){
    url = 'https://s3.amazonaws.com/mazkaracdn/' + url;
    return url;
  };


  indexCtrl.init = function(){
    url = siteApiRoot+'api/base/web/data/homepage';
    url = '/app/data/home-page.json';
    //url = 'http://fabogosandbox-dev.ap-northeast-1.elasticbeanstalk.com/api/searchForWeb';
    $http.post(url, jQuery.param([]), {
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }
    ).success(function(data){
      indexCtrl.popularServices = data.data.popularServices
      indexCtrl.featuredVenues = data.data.featuredVenues
      indexCtrl.latestPosts = data.data.latestPosts
      indexCtrl.city = data.data.city
      sharedParams = indexCtrl.city;
    });

  };
  indexCtrl.init();



});

