fabogoApp.controller('JobsController', 
  function($scope, $http, $log, $routeParams, $route, sharedParams){
    var jthis = this;
    var sharedParams = sharedParams;

    // single venue page variables
    jthis.jobs = [];
    jthis.cities = [];
    jthis.selectedCity = 'dubai';


    jthis.init = function(){
      jthis.getJobs();
    };

    jthis.getJobs = function(){
      url = '/app/data/jobs.json';

      $http.post(url, {}, {
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }
      ).success(function(data){
        jthis.jobs = data.jobs;
        jthis.cities = data.cities;
        jthis.initialized = 1;
      });

    };

    jthis.init();



});



