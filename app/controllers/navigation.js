fabogoApp.controller('NavigationController', function($scope, $http, $log, $routeParams, $route, sharedParams){
  var navCtrl = this;
  navCtrl.menu = [];

  navCtrl.header_images = function(slug){
    url = 'https://s3.amazonaws.com/mazkaracdn/assets/'+slug+'.jpg';
    return url;
  }

  navCtrl.getCurrentCitySlug = function(){
    city = sharedParams.getCurrentCity();
    return city.slug;
  }

  navCtrl.getCurrentCategorySlug = function(){
    category = sharedParams.getCurrentCategory();
    
    category = category.slug.replace(/s$/, '');
    return category;
  }

  navCtrl.setupMenu = function(){
    
    url = siteApiRoot+'api/base/web/services/for/navigation';
    url = '/app/data/services-navigation.json';
    //url = 'http://fabogosandbox-dev.ap-northeast-1.elasticbeanstalk.com/api/searchForWeb';
    $http.post(url, jQuery.param([]), {
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      }
    ).success(function(data){

      for(i in data.data){
        children = []
        x = 0;
        children[x] = [];
        for(j in data.data[i].children){
          if(children[x].length==8){
            x++;
            children[x] = [];
          }
          children[x].push(data.data[i].children[j]);
        }
        data.data[i].children = children;
      }
      navCtrl.menu = data.data;
    });
  }

  navCtrl.init = function () {
    navCtrl.setupMenu();
  };

  navCtrl.init();

});
