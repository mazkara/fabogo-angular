fabogoApp.controller('StoriesController', 
  function($scope, $http, $log, $routeParams, $route, sharedParams){
    var sthis = this;
    var sharedParams = sharedParams;

    // single venue page variables
    sthis.stories = [];
    sthis.filter = { service:null, search:'' };
    sthis.services = [];
    sthis.suggested_posts = [];

    sthis.totalItems = 10;
    sthis.currentPage = 1;
    sthis.maxSize = 8;
    sthis.numPerPage = 20;
    sthis.totalPages = 0;
    sthis.initialized  = 0;

    sthis.init = function(){


      sthis.services = sharedParams.getServices();
      console.log(sthis.services);
      sthis.getPosts();
    };

    sthis.getPosts = function(){
      console.log(2);
      data = sthis.getFilterParameters();
      url = '/app/data/posts.json';

      $http.post(url, jQuery.param(data), {
          method: "POST",
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }
      ).success(function(data){
        sthis.stories = data.posts.data;
        sthis.suggested_posts = data.suggested_posts;
        sthis.totalItems = data.posts.total;
        sthis.currentPage = data.posts.current_page;
        sthis.initialized = 1;
      });

    };

    sthis.getFilterParameters = function(){
      data = {};

      data['batchNumber'] = sthis.currentPage;
      data['queryText'] = sthis.filter.search;

      if(sthis.filter.service!=null){
        data['service'] = sthis.filter.service;
      }

      data['session_search_page'] =  sthis.session_search_page;
      data['session_search_parameters'] =  sthis.session_search_parameters;
      return data;
    }

    sthis.pageChanged = function() {
      sthis.getPosts();
    };

    sthis.mzk_single_story_link = function(story){
      return '/stories/'+story.id+'/'+story.slug;
    }

    sthis.mzk_stories_link = function(params){
      return '/stories/'+params.service.slug;
    }
    sthis.init();



});



